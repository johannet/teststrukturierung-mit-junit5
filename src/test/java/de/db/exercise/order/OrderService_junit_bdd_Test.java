package de.db.exercise.order;

import de.db.order.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SuppressWarnings({"NewClassNamingConvention", "OptionalUsedAsFieldOrParameterType"})
@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class OrderService_junit_bdd_Test {

    final static String CUSTOMER_ID = "someUserId";
    final static List<String> PRODUCTS = List.of("someProductId", "otherProductId", "yetAnotherProductId");

    OrderService sut;

    @Mock
    OrderRepository orderRepository;

    @Mock
    CustomerService customerService;

    @Mock
    StockService stockService;

    @Mock
    EventBus eventBus;

    @BeforeEach
    void setUp() {
        sut = new OrderService(orderRepository, customerService, stockService, eventBus);
    }

    @Nested
    class Given_customer_does_not_exist {

        @BeforeEach
        void arrange() {
            when(customerService.customerExists(CUSTOMER_ID)).thenReturn(false);
        }

        @Nested
        class When_calling_placeOrder {

            private Optional<OrderSummary> result;

            @BeforeEach
            void act() {
                result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);
            }

            @Test
            void then_result_is_empty() {
                assertThat(result).isEmpty();
            }

            @Test
            void then_no_order_is_persisted() {
                verifyNoInteractions(orderRepository);
            }

            @Test
            void then_no_order_is_registered() {
                verify(customerService, never()).registerOrder(anyString(), anyString());
            }

            @Test
            void then_stockService_is_not_queried() {
                verifyNoInteractions(stockService);
            }

            @Test
            void then_no_event_is_published() {
                verifyNoInteractions(eventBus);
            }
        }
    }

    // TODO 1 - Testen was passiert wenn der Nutzer existiert, aber ein Produkt nicht verfügbar ist

    // TODO 2 - Testen was passiert wenn der Nutzer existiert und alle Produkte verfügbar sind
}
