package de.db.exercise.order;

import com.tngtech.jgiven.Stage;
import com.tngtech.jgiven.annotation.BeforeStage;
import com.tngtech.jgiven.annotation.ExpectedScenarioState;
import com.tngtech.jgiven.annotation.ProvidedScenarioState;
import com.tngtech.jgiven.annotation.Quoted;
import com.tngtech.jgiven.junit5.JGivenExtension;
import com.tngtech.jgiven.junit5.ScenarioTest;
import de.db.order.*;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SuppressWarnings({"NewClassNamingConvention", "OptionalUsedAsFieldOrParameterType"})
@ExtendWith(JGivenExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class OrderService_jgiven_bdd_Test extends ScenarioTest<
        OrderService_jgiven_bdd_Test.GivenStage,
        OrderService_jgiven_bdd_Test.WhenStage,
        OrderService_jgiven_bdd_Test.ThenStage
        > {

    final static String CUSTOMER_ID = "someUserId";
    final static List<String> PRODUCTS = List.of("someProductId", "otherProductId", "yetAnotherProductId");

    @Test
    void test_missing_user() {
        given().customer_with_id("otherCustomerId");

        when().calling_placeOrder_for_customer_$_ordering_products(CUSTOMER_ID, PRODUCTS);

        then().result_is_empty()
                .and().order_is_not_persisted()
                .and().order_is_not_registered()
                .and().stockService_is_not_queried()
                .and().no_event_is_published();
    }

    // TODO 1 - Testen was passiert wenn der Nutzer existiert, aber ein Produkt nicht verfügbar ist

    // TODO 2 - Testen was passiert wenn der Nutzer existiert und alle Produkte verfügbar sind

    static class GivenStage extends Stage<GivenStage> {

        @ProvidedScenarioState
        OrderService sut;

        @ProvidedScenarioState
        OrderRepository orderRepository;

        @ProvidedScenarioState
        CustomerService customerService;

        @ProvidedScenarioState
        StockService stockService;

        @ProvidedScenarioState
        EventBus eventBus;

        GivenStage customer_with_id(@Quoted String customerId) {
            Mockito.when(customerService.customerExists(customerId)).thenReturn(true);
            return self();
        }

        @BeforeStage
        void setUp() {
            orderRepository = Mockito.mock(OrderRepository.class);
            customerService = Mockito.mock(CustomerService.class);
            stockService = Mockito.mock(StockService.class);
            eventBus = Mockito.mock(EventBus.class);
            sut = new OrderService(orderRepository, customerService, stockService, eventBus);
        }
    }

    static class WhenStage extends Stage<WhenStage> {

        @ExpectedScenarioState
        OrderService sut;

        @ProvidedScenarioState
        Optional<OrderSummary> result;

        WhenStage calling_placeOrder_for_customer_$_ordering_products(
                @Quoted String customerId,
                List<String> products
        ) {
            result = sut.placeOrder(products, customerId);
            return self();
        }
    }

    static class ThenStage extends Stage<ThenStage> {
        @ExpectedScenarioState
        Optional<OrderSummary> result;

        @ExpectedScenarioState
        OrderRepository orderRepository;

        @ExpectedScenarioState
        CustomerService customerService;

        @ExpectedScenarioState
        StockService stockService;

        @ExpectedScenarioState
        EventBus eventBus;

        ThenStage result_is_empty() {
            assertThat(result).isEmpty();
            return self();
        }

        ThenStage order_is_not_persisted() {
            verifyNoInteractions(orderRepository);
            return self();
        }

        ThenStage order_is_not_registered() {
            verify(customerService, never()).registerOrder(anyString(), anyString());
            return self();
        }

        ThenStage stockService_is_not_queried() {
            verifyNoInteractions(stockService);
            return self();
        }

        ThenStage no_event_is_published() {
            verifyNoInteractions(eventBus);
            return self();
        }
    }
}
