package de.db.complete.order;

import de.db.order.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@SuppressWarnings({"NewClassNamingConvention", "OptionalUsedAsFieldOrParameterType"})
@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class OrderService_junit_bdd_Test {

    final static String CUSTOMER_ID = "someUserId";
    final static List<String> PRODUCTS = List.of("someProductId", "otherProductId", "yetAnotherProductId");

    OrderService sut;

    @Mock
    OrderRepository orderRepository;

    @Mock
    CustomerService customerService;

    @Mock
    StockService stockService;

    @Mock
    EventBus eventBus;

    @BeforeEach
    void setUp() {
        sut = new OrderService(orderRepository, customerService, stockService, eventBus);
    }

    @Nested
    class Given_customer_does_not_exist {

        @BeforeEach
        void arrange() {
            when(customerService.customerExists(CUSTOMER_ID)).thenReturn(false);
        }

        @Nested
        class When_calling_placeOrder {

            private Optional<OrderSummary> result;

            @BeforeEach
            void act() {
                result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);
            }

            @Test
            void then_result_is_empty() {
                assertThat(result).isEmpty();
            }

            @Test
            void then_no_order_is_persisted() {
                verifyNoInteractions(orderRepository);
            }

            @Test
            void then_no_order_is_registered() {
                verify(customerService, never()).registerOrder(anyString(), anyString());
            }

            @Test
            void then_stockService_is_not_queried() {
                verifyNoInteractions(stockService);
            }

            @Test
            void then_no_event_is_published() {
                verifyNoInteractions(eventBus);
            }
        }
    }

    @Nested
    class Given_customer_exists {

        @BeforeEach
        void arrange() {
            when(customerService.customerExists(CUSTOMER_ID)).thenReturn(true);
        }

        @Nested
        class Given_products_are_out_of_stock {

            @BeforeEach
            void arrange() {
                when(stockService.isInStock(PRODUCTS.get(0))).thenReturn(true);
                when(stockService.isInStock(PRODUCTS.get(1))).thenReturn(true);
                when(stockService.isInStock(PRODUCTS.get(2))).thenReturn(false);
            }

            @Nested
            class When_calling_placeOrder {

                private Optional<OrderSummary> result;

                @BeforeEach
                void act() {
                    result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);
                }

                @Test
                void then_result_is_empty() {
                    assertThat(result).isEmpty();
                }

                @Test
                void then_no_order_is_persisted() {
                    verifyNoInteractions(orderRepository);
                }

                @Test
                void then_no_order_is_registered() {
                    verify(customerService, never()).registerOrder(anyString(), anyString());
                }

                @Test
                void then_no_event_is_published() {
                    verifyNoInteractions(eventBus);
                }
            }
        }

        @Nested
        class Given_products_are_in_stock {

            @BeforeEach
            void arrange() {
                when(stockService.isInStock(PRODUCTS.get(0))).thenReturn(true);
                when(stockService.isInStock(PRODUCTS.get(1))).thenReturn(true);
                when(stockService.isInStock(PRODUCTS.get(2))).thenReturn(true);
            }

            @Nested
            class Given_repository_saves_order_with_new_id {

                @BeforeEach
                void arrange() {
                    when(orderRepository.save(new OrderSummary(null, PRODUCTS)))
                            .thenReturn(new OrderSummary("someOrderId", PRODUCTS));
                }

                @Nested
                class When_calling_placeOrder {

                    private Optional<OrderSummary> result;

                    @BeforeEach
                    void act() {
                        result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);
                    }

                    @Test
                    void then_order_is_persisted() {
                        verify(orderRepository).save(new OrderSummary(null, PRODUCTS));
                    }

                    @Test
                    void then_order_summary_is_returned_with_id() {
                        assertThat(result).hasValue(new OrderSummary("someOrderId", PRODUCTS));
                    }

                    @Test
                    void then_order_is_registered() {
                        verify(customerService).registerOrder(CUSTOMER_ID, "someOrderId");
                    }

                    @Test
                    void then_order_event_is_published() {
                        verify(eventBus).publish(new OrderEvent(CUSTOMER_ID, "someOrderId"));
                    }
                }
            }
        }
    }
}
