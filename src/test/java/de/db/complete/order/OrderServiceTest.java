package de.db.complete.order;

import de.db.order.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class OrderServiceTest {

    final static String CUSTOMER_ID = "someUserId";
    final static List<String> PRODUCTS = List.of("someProductId", "otherProductId", "yetAnotherProductId");

    OrderService sut;

    @Mock
    OrderRepository orderRepository;

    @Mock
    CustomerService customerService;

    @Mock
    StockService stockService;

    @Mock
    EventBus eventBus;

    @BeforeEach
    void setUp() {
        sut = new OrderService(orderRepository, customerService, stockService, eventBus);
    }

    @Test
    void returns_empty_orderSummary_when_customer_does_not_exist_and_has_no_side_effect() {
        // Arrange
        when(customerService.customerExists(CUSTOMER_ID)).thenReturn(false);

        // Act
        var result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);

        // Assert
        assertThat(result).isEmpty();
        verifyNoInteractions(orderRepository);
        verify(customerService, never()).registerOrder(anyString(), anyString());
        verifyNoInteractions(stockService);
        verifyNoInteractions(eventBus);
    }

    @Test
    void returns_empty_orderSummary_when_customer_exists_products_are_out_of_stock_and_has_no_side_effect() {
        // Arrange
        when(customerService.customerExists(CUSTOMER_ID)).thenReturn(true);
        when(stockService.isInStock(PRODUCTS.get(0))).thenReturn(true);
        when(stockService.isInStock(PRODUCTS.get(1))).thenReturn(true);
        when(stockService.isInStock(PRODUCTS.get(2))).thenReturn(false);

        // Act
        var result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);

        // Assert
        assertThat(result).isEmpty();
        verifyNoInteractions(orderRepository);
        verify(customerService, never()).registerOrder(anyString(), anyString());
        verifyNoInteractions(eventBus);
    }

    @Test
    void returns_orderSummary_when_customer_exists_products_are_in_stock_and_has_side_effects() {
        // Arrange
        when(customerService.customerExists(CUSTOMER_ID)).thenReturn(true);
        when(stockService.isInStock(PRODUCTS.get(0))).thenReturn(true);
        when(stockService.isInStock(PRODUCTS.get(1))).thenReturn(true);
        when(stockService.isInStock(PRODUCTS.get(2))).thenReturn(true);
        when(orderRepository.save(new OrderSummary(null, PRODUCTS)))
                .thenReturn(new OrderSummary("someOrderId", PRODUCTS));

        // Act
        var result = sut.placeOrder(PRODUCTS, CUSTOMER_ID);

        // Assert
        assertThat(result).hasValue(new OrderSummary("someOrderId", PRODUCTS));
        verify(orderRepository).save(new OrderSummary(null, PRODUCTS));
        verify(customerService).registerOrder(CUSTOMER_ID, "someOrderId");
        verify(eventBus).publish(new OrderEvent(CUSTOMER_ID, "someOrderId"));
    }
}
