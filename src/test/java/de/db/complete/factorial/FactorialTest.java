package de.db.complete.factorial;

import de.db.factorial.Factorial;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.Method;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayNameGeneration(FactorialTest.MyDisplayNameGenerator.class)
class FactorialTest {

    @Test
    void calculate_returns_6_for_input_of_3() {
        // Arrange
        var factorial = new Factorial();

        // Act
        var result = factorial.calculate(3);

        // Assert
        assertThat(result).isEqualTo(6);
    }

    @Test
    void calculate_returns_120_for_input_of_5() {
        // Arrange
        var factorial = new Factorial();

        // Act
        var result = factorial.calculate(5);

        // Assert
        assertThat(result).isEqualTo(120);
    }

    @ParameterizedTest(name = "calculate({0}) => {1}")
    @MethodSource("factorialTestValues")
    void calculate_returns_factorial_for_input(long input, long expectedResult) {
        // Arrange
        var factorial = new Factorial();

        // Act
        var result = factorial.calculate(input);

        // Assert
        assertThat(result).isEqualTo(expectedResult);
    }

    public static Stream<Arguments> factorialTestValues() {
        return Stream.of(
                Arguments.of(0, 1),
                Arguments.of(1, 1),
                Arguments.of(2, 2),
                Arguments.of(3, 6),
                Arguments.of(4, 24),
                Arguments.of(5, 120),
                Arguments.of(6, 720)
        );
    }

    static class MyDisplayNameGenerator extends DisplayNameGenerator.Standard {

        @Override
        public String generateDisplayNameForClass(Class<?> testClass) {
            return "Testing " + super.generateDisplayNameForClass(testClass).replace("Test", "");
        }

        @Override
        public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
            return testMethod.getName()
                    .replaceFirst(
                            "calculate_returns_(.+)_for_input_of_(.+)",
                            "calculate($2) => $1"
                    );
        }
    }
}
