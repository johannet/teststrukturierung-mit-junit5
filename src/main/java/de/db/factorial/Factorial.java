package de.db.factorial;

public class Factorial {

    public long calculate(long input) {
        long result = 1;

        for (long factor = 2; factor <= input; factor++) {
            result *= factor;
        }

        return result;
    }
}
