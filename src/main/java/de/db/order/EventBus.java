package de.db.order;

public interface EventBus {

    void publish(OrderEvent orderEvent);
}
