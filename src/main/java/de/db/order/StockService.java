package de.db.order;

public interface StockService {

    boolean isInStock(String productId);
}
