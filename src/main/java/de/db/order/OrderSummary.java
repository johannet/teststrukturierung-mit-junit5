package de.db.order;

import java.util.List;
import java.util.Objects;

public class OrderSummary {

    private final String orderId;
    private final List<String> productIds;

    public OrderSummary(String orderId, List<String> productIds) {
        this.orderId = orderId;
        this.productIds = productIds;
    }

    public String getOrderId() {
        return orderId;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    @Override
    public String toString() {
        return "OrderSummary{" +
                "orderId='" + orderId + '\'' +
                ", productIds=" + productIds +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderSummary that = (OrderSummary) o;
        return Objects.equals(orderId, that.orderId) && Objects.equals(productIds, that.productIds);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, productIds);
    }
}
