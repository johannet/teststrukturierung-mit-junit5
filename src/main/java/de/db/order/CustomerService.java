package de.db.order;

public interface CustomerService {

    void registerOrder(String customerId, String orderId);

    boolean customerExists(String customerId);
}
