package de.db.order;

public interface OrderRepository {

    OrderSummary save(OrderSummary orderSummary);
}
