package de.db.order;

import java.util.List;
import java.util.Optional;

public class OrderService {

    private final OrderRepository orderRepository;
    private final CustomerService customerService;
    private final StockService stockService;
    private final EventBus eventBus;

    public OrderService(
            OrderRepository orderRepository,
            CustomerService customerService,
            StockService stockService,
            EventBus eventBus
    ) {
        this.orderRepository = orderRepository;
        this.customerService = customerService;
        this.stockService = stockService;
        this.eventBus = eventBus;
    }

    public Optional<OrderSummary> placeOrder(List<String> productIds, String customerId) {
        if (!customerService.customerExists(customerId)) {
            return Optional.empty();
        }

        if (isAnyProductOutOfStock(productIds)) {
            return Optional.empty();
        }

        var orderSummary = new OrderSummary(null, productIds);
        orderSummary = orderRepository.save(orderSummary);

        customerService.registerOrder(customerId, orderSummary.getOrderId());
        eventBus.publish(new OrderEvent(customerId, orderSummary.getOrderId()));

        return Optional.of(orderSummary);
    }

    private boolean isAnyProductOutOfStock(List<String> productIds) {
        return !productIds.stream().allMatch(stockService::isInStock);
    }
}
