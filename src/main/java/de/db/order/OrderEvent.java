package de.db.order;

import java.util.Objects;

public class OrderEvent {

    private final String orderId;
    private final String customerId;

    public OrderEvent(String customerId, String orderId) {
        this.orderId = orderId;
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderEvent that = (OrderEvent) o;
        return Objects.equals(orderId, that.orderId) && Objects.equals(customerId, that.customerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, customerId);
    }
}
