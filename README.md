# Teststrukturierung-mit-junit5

Enthält eine Reihe von Beispielen und Übungsaufgaben wie Tests mit JUnit 5 strukturiert werden können.
Neben JUnit 5 kommen auch JGiven, AssertJ und Mockito zum Einsatz.

## Linksammlung
- JUnit - <https://junit.org/junit5/>
- JGiven - <https://jgiven.org>
- AssertJ - <https://assertj.github.io/doc/>
- Mockito - <https://site.mockito.org>
